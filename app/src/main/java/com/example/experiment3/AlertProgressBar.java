package com.example.experiment3;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.NonNull;
import butterknife.ButterKnife;

public class AlertProgressBar extends AlertDialog {

    private int progressIndex=0;
    private ProgressBar progressBar;
    private TextView textView1,textView2;
    private LinearLayout linearLayout,rateLinear;
    private Handler handler;
    private Timer timer;
    private TimerTask timerTask;
    private int period=100;
    private Button btnClose;

    public AlertProgressBar(@NonNull Context context) {
        super(context);
        ButterKnife.bind(this);
        progressBar=new ProgressBar(context,null,android.R.attr.progressBarStyleHorizontal);
        progressBar.setMax(100);
        textView1=new TextView(context);
        textView2=new TextView(context);
        linearLayout=new LinearLayout(context);
        rateLinear=new LinearLayout(context);
        btnClose=new Button(context);

        initLinearLayout();
        initDialog();

    }


    private void initDialog(){

        setView(linearLayout);
        setCanceledOnTouchOutside(false);

        setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                timer.cancel();
                timer=null;
                timerTask.cancel();
                timerTask=null;
                progressIndex=0;
            }
        });
    }

    private void initLinearLayout(){


        linearLayout.setOrientation(LinearLayout.VERTICAL);
        rateLinear.setOrientation(LinearLayout.HORIZONTAL);

        linearLayout.addView(progressBar);
        linearLayout.addView(rateLinear);
        linearLayout.addView(btnClose);
        rateLinear.addView(textView1);
        rateLinear.addView(textView2);

        textView2.setLeft(50);
        textView2.setPadding(50,0,0,0);

        LinearLayout.LayoutParams layoutParams=(LinearLayout.LayoutParams)btnClose.getLayoutParams();
        layoutParams.setMargins(200,0,200,0);
        btnClose.setLayoutParams(layoutParams);
        btnClose.setText("关闭");
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

    }

    public void showProgreeDialog(){
        show();
        progressRun();
    }

    private void progressRun(){
        handler=new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                if(msg.what==1) {
                    progressBar.setProgress(progressIndex++);
                    String rate1=String.valueOf(progressIndex)+"%";
                    String rate2=String.valueOf(progressIndex)+"/100";
                    textView1.setText(rate1);
                    textView2.setText(rate2);
                }
                else if(msg.what==0){
                    textView1.setText("");
                    textView2.setText("任务完成");
                }
                return true;
            }
        });
        timer=new Timer();
        timerTask=new TimerTask() {
            @Override
            public void run() {
                Message message=new Message();
                if(progressIndex>=99) {
                    message.what=0;
                }
                else
                    message.what=1;
                handler.sendMessage(message);

            }
        };
        timer.schedule(timerTask,0,period);
    }
    public void setPeriod(int period){
        this.period=period;
    }
}
