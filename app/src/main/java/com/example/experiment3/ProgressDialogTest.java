package com.example.experiment3;


import androidx.appcompat.app.AppCompatActivity;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.Activity;

import android.os.Bundle;

import android.view.View;




public class ProgressDialogTest extends AppCompatActivity {

    AlertProgressBar alertProgressBar;

    @OnClick(R.id.taskbtn)
    void showProgreeDialog(View v){
        alertProgressBar.showProgreeDialog();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progress_dialog_test);
        ButterKnife.bind(this);
        alertProgressBar=new AlertProgressBar(this);
        alertProgressBar.setTitle("任务完成百分比");
        alertProgressBar.setMessage("耗时任务完成的百分比");
        alertProgressBar.setPeriod(100);
    }



}
