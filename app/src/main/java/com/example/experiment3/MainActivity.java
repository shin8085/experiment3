package com.example.experiment3;

import android.app.Activity;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView mtextView;

    @OnClick(R.id.mbtn6)
    void mbtn6OnClick(View v){
        mtextView.setText("采用了ButterKnife绑定的监听器");
    }

    @OnClick(R.id.mbtn7)
    void toConsfigurationTest(View v){
        Intent intent=new Intent();
        intent.setClass(this,ConfigurationTest.class);
        startActivity(intent);
    }

    @OnClick(R.id.mbtn8)
    void toProgressDialogTest(View v){
        Intent intent=new Intent();
        intent.setClass(this,ProgressDialogTest.class);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        mtextView=findViewById(R.id.mtextView);

        //采用Activity作为监听器
        findViewById(R.id.mbtn1).setOnClickListener(this);

        //匿名内部类
        findViewById(R.id.mbtn2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mtextView.setText("点击了采用匿名内部类绑定的监听器");

            }
        });

        //内部类
        class mbtn4OnClick implements View.OnClickListener{

            @Override
            public void onClick(View v) {
                mtextView.setText("点击了采用内部类绑定的监听器");
            }
        }
        findViewById(R.id.mbtn4).setOnClickListener(new mbtn4OnClick());

        //外部类
        findViewById(R.id.mbtn5).setOnClickListener(new mbtn5OnClick(mtextView));


    }

    @Override
    public void onClick(View v) {
        mtextView.setText("点击了采用Activity作为监听器绑定的监听器");
    }
    //绑定到标签
    public void onmbt3Click(View v){
        mtextView.setText("点击了采用标签绑定的监听器");
    }

}
class mbtn5OnClick implements View.OnClickListener{

    TextView textView;
    public mbtn5OnClick(TextView textView){
        this.textView=textView;
    }

    @Override
    public void onClick(View v) {
        textView.setText("点击了采用外部类绑定的监听器");
    }
}
