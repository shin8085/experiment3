package com.example.experiment3;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class ConfigurationTest extends AppCompatActivity {

    @BindViews({R.id.cedit1,R.id.cedit2,R.id.cedit3,R.id.cedit4})
    EditText editText [];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuration_test);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.cbtn)
    void getInfo(View v){
        Configuration configuration=this.getResources().getConfiguration(); //获取配置信息

        String orientation=configuration.orientation==Configuration.ORIENTATION_LANDSCAPE?"横向屏幕":"竖向屏幕";

        String navigation;
        switch (configuration.navigation){
            case Configuration.NAVIGATION_WHEEL:
                navigation="滚轮控制方向";break;
            case Configuration.NAVIGATION_NONAV:
                navigation="没有方向控制";break;
            case Configuration.NAVIGATION_TRACKBALL:
                navigation="轨迹球控制方向";break;
            case Configuration.NAVIGATION_DPAD:
                navigation="方向键控制方向";break;
            case Configuration.NAVIGATION_UNDEFINED:
                default:
                    navigation="未知";
        }

        String touchscreen;
        switch (configuration.touchscreen){
            case Configuration.TOUCHSCREEN_FINGER:
                touchscreen="允许手指触摸";break;
            case Configuration.TOUCHSCREEN_NOTOUCH:
                touchscreen="不支持触摸";break;
            case Configuration.TOUCHSCREEN_UNDEFINED:
            default:
                touchscreen="未知";
        }

        int mnc=configuration.mnc;

        editText[0].setText(orientation);
        editText[1].setText(navigation);
        editText[2].setText(touchscreen);
        editText[3].setText(String.valueOf(mnc));
    }
}
